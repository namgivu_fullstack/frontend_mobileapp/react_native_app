var findSmallestSetOfVertices = function(n, edges) {
    var array = []
    var count = 0;
    console.log("Edges: " + edges);
    for(var tmp = 0; tmp < n; tmp++){
        for(var j = 0; j < edges.length; j++){
            if(edges[j][0] == tmp){
                array[count] = tmp;
                count++;
                break;
            }
        }
    }
    console.log("Array: " + array)
    var index_;
    for(var tmp = 0; tmp < n; tmp++){
        for(var j = 0; j < edges.length; j++){
            if(edges[j][1] === array[tmp]){
                index_ = array.indexOf(array[tmp]);
                array.splice(index_, 1);
                tmp = index_ - 1;
                break;
            }
        }
    }
    return array;
};

var n = 5;
var edges=
        [[0,1],[3,2],[4,1],[2,1],[4,2],[3,4],[0,2],[4,0]]
console.log(findSmallestSetOfVertices(n, edges));