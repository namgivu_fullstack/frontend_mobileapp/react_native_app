var swapPairs = function(head) {
    for (var i = 0; i < head.length; i++){
        if((i % 2) !== 0){
            var tmp = head[i - 1];
            head[i - 1] = head[i]
            head[i] = tmp
        }
    }
    return head;
};

var head_ = [1,2,3,4]
var http = require('http')


http.createServer(function (rep, res) {
    res.writeHead(200, {'Content-Type': 'text/html'});
    res.write("head: " + head_);
    res.write(" => Swap: " + swapPairs(head_));
    res.end();
}).listen(8080);