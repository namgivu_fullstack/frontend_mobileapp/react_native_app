/**
 * @param {string} s
 * @return {boolean}
 */
var isValid = function(s) {
    var stack = []
    for (var c of s){
        if(c === '(' || c === '[' || c === '{'){
            stack.push(c)
        }else{
            if((c === ')' && stack.pop() !== '(') ||
                (c === ']' && stack.pop() !== '[') ||
                (c === '}' && stack.pop() !== '{')){
                return false;
            }
            // stack.pop() // để xóa giá trí cũ rồi khi push giá trị mới nhầm xét theo từng cặp
        }
        console.log(stack)
    }
    console.log(stack.length)
    return !stack.length;
    //stack.lenght là có độ dài khác với nó là ko có độ dài tức false và ngược lại
    // nếu có độ dài tức là trong stack đang chứa  cặp ngoặc ko bị loại bỏ tức là ngoặc đó bị thiếu hoặc bị sai trong chuỗi string truyền vào
};

var str = '(){{{}}]' // true or false ??

//Check isValid()
console.log(isValid(str));