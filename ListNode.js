class Node{
    constructor(element) {
        // element is data
        // next is pointer
        this.element = element;
        this.next = null;
    }
}
class List {
    // list includes node head and length
    constructor(head) {
        this.head = head;
        this.size = 0;
    }

    // add node
    // inset at index
    add(element){
        var node = new Node(element);
        var current;

        if(this.head == null){
            this.head = node;
        }else{
            current = this.head;
            while (current.next){
                current = current.next;
            }
            current.next = node;
        }
        this.size++;
    }

    insertAt(element, index){
        var node = new  Node(element);
        var current, previous;

        if (index == 0){
            node.next = this.head;
            this.head = node;
        }else{
            current = this.head;
            var it = 0;

            while ( it < index){
                it++;
                previous = current;
                current = current.next;
            }
            previous.next = node;
            node.next = current;
        }
        this.size++;
    }

    removeForm(index){
        if (index < 0 || index >= this.size){

        }else{
            var current = this.head;
            var previous;
            var it = 0;

            if(index === 0){
                this.head = current.head;
            }else{
                while(it < index){
                    it++;
                    previous = current;
                    current = current.next;
                }
                previous.next = current.next;
            }
            this.size--;
        }
        return current.element;
    }

    removeElement(element){
    //     element ở đây chỉ là 1 data
        var current = this.head;
        var previous = null;

        while(current != null){
            if(current.element === element){
                if(previous == null){
                    this.head = current.next;
                }else{
                    prev.next = current.next;
                }
                this.size--;
                return current.element; // current.element = null -> true
            }
            previous = current;
            current = current.next;
        }
        return -1;
    }

}