/**
 * @param {number[][]} graph
 * @return {boolean}
 */
var isBipartite = function(graph) {
    var check = true;
    if (graph == null || graph.length == null) {
        return;
    }

    // graph = Array.from(new Set(graph));
    for (var z = 0; z < graph.length; z++) {
        for (var i = z + 1; i < graph.length; i++) {
            for (var j = 0; graph[z][j] != null; j++) {
                for (var e = 1; graph[i][e] != null; e++) {
                    if (graph[z][j] === graph[i][e]) {
                        if(j !== e){
                            check = false;
                        }
                    }
                }
            }
        }
    }
    return check;
};

graph = [[1,2,3],[0,2],[0,1,3],[0,2]]
// graph =   [[1,3],[0,2],[1,3],[0,2]]
console.log(isBipartite(graph))
