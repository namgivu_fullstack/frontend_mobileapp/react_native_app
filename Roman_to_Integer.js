/**
 * @param {string} s
 * @return {number}
 */
var romanToInt = function(s) {
    const symbol = {
        'I': 1,
        'V': 5,
        'X': 10,
        'L': 50,
        'C': 100,
        'D': 500,
        'M': 1000,
    }

    const special = {
        'IV': 4,
        'IX': 9,
        'XL': 40,
        'XC': 90,
        'CD': 400,
        'CM': 900,
    }
    var arrNum = []

    for(var i = 0; i < s.length; i++){
        for (var j = 0; j < 7; j++){
            if(s[i] === Object.keys(symbol)[j]){
                var tmp = i
                if(s[i] === 'I' || s[i] === 'X' || s[i] === 'C'){
                    if(i !== s.length - 1){
                        var num = s[i] + s[i+1]
                        for(var z = 0; z < 6; z++){
                            if(num === Object.keys(special)[z]){
                                arrNum.push(special[num])
                                i = i + 1;
                                console.log(i + "ok")
                                break;
                            }
                        }
                    }
                }
                console.log("tmp " + tmp)
                if(tmp === i){
                    arrNum.push(symbol[s[i]])
                    break;
                }else{
                    break;
                }
            }
        }

        // console.log(symbol[s[i]]);
    }
    console.log(arrNum);

    var sum = 0;
    for (var i = 0; i < arrNum.length; i++){
        sum += arrNum[i]
    }

    return sum
};