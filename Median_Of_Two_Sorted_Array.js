var findMedianSortedArrays = function(nums1, nums2) {

    var nums = nums1.concat(nums2)
    if(nums == null || nums.length == null){
        return nums;
    }
    for (var i = 0; i < nums.length; i++){
        for (var j = i + 1; j <= nums.length; j++){
            if(nums[i] > nums[j]){
                var tmp = nums[i];
                nums[i] = nums[j];
                nums[j] = tmp;
            }
        }
    }

    console.log("Array is sorted: " + nums);
    console.log("Length: " +nums.length);

    var median = 0;
    if (nums.length % 2 !== 0){
        var index = ((nums.length - 1) / 2);
        console.log("Index of median: " + index);
        median = nums[index];
    }else{
        var index = nums.length / 2;
        console.log("Index of median: " + index)
        median = (nums[index - 1] + nums[index]) /2;
    }
    return median
};

var nums1 = [1,3,4,6,8,9]
var nums2 = [2, 7, 5]

var http = require('http')
http.createServer(function (rep, res) {
    res.writeHead(200, {'Content-Type': 'text/html'});
    res.write("Nums1: " + nums1 + " / " + "Nums2: " + nums2);
    res.write(" => Median is: " + findMedianSortedArrays(nums1, nums2));
    res.end();
}).listen(8080);

console.log("Median is: " + findMedianSortedArrays(nums1, nums2))