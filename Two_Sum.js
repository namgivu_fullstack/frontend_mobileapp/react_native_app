var http = require('http');
var twoSum = function(nums, target) {
    var tmp =  0
    var id1 = 0;
    var id2 = 0;
    for (var i = 0; i < nums.length; i++){
        for (var j = i + 1; j < nums.length; j++){
            tmp = nums[i] + nums[j];
            if (tmp === target){
                id1 = i;
                id2 = j;
                break;
            }
        }
    }
    return id1 + " " + id2;
};
var nums_ = [3,2,4]

http.createServer(function (req, res) {
    res.writeHead(200, {'Content-Type': 'text/html'});
    res.write("Resul: "+ twoSum(nums_, 6));
    res.end();
}).listen(8080);